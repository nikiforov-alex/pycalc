"""
module, which work whith arguments command line
"""
import argparse


def create_parser():
    """
    function, which create parser, for given arguments
    :return:
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('expression', help='write expression, which been calculate', type=str)

    parser.add_argument('-u', '--use-modules', nargs='+',
                        help='modules, which may uses in your expression')
    args = parser.parse_args()
    return args


if __name__ == '__main__':
    create_parser()
