"""
module, which work with constants and functions in expression
"""
import os
import sys

from pycalc.pycalc_helper import MODULES


def calculate_const(operand):
    """
    function, which prepare constants
    :param operand: supposed constant
    :return: answer(int/float)
    """
    res = ''
    true_flag = False
    for module in MODULES:
        try:
            res = calculate_const_func(module, operand)
            true_flag = True
            break
        except AttributeError:
            pass
    if not true_flag:
        raise ArithmeticError('error')
    return res


def calculate_func(may_func, interval, expression, modules, args):
    """
    function, which prepare functions in expression
    :param interval: interval in expression
    :param args: args functions
    :param may_func: supposed functions
    :param expression: main expression
    :param modules: modules
    :return:
    """
    true_flag = False
    res = ''
    for module in modules:
        try:
            res = calculate_const_func(module, may_func, args)
            true_flag = True
            break
        except AttributeError:
            pass
    if not true_flag:
        raise ArithmeticError('error')
    delta = interval[1] - interval[0] - len(str(res))
    expression = expression[:interval[0]] + str(res) + expression[interval[1] + 1:]
    i = interval[1] - delta
    return expression, i


def calculate_const_func(module, func_const, *args):
    """
    function, which calculate functions and constants
    :param module:
    :param func_const:
    :param args:
    :return:
    """
    ans = None
    try:
        mod = __import__(module)
    except ImportError:
        sys.path.insert(0, os.path.abspath('.'))
        mod = __import__(module)
    issue = getattr(mod, func_const)
    if 'function' in str(type(issue)):
        ans = issue(*args)
    else:
        ans = issue
    return ans
