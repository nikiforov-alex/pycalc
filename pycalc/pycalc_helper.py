"""
module, which contains functions-helper
"""
import re

from termcolor import colored

# key represent priority of operators
OPERATORS = {6: ['**', '^'],
             5: ['+@', '-@'],
             4: ['*', '/', '%', '//'],
             3: ['+', '-'],
             2: ['<=', '>=', '<', '>'],
             1: ['<>', '==', '!=']}

MODULES = ['builtins', 'math']


def my_exchandler(type_exc, value, traceback):
    """
    redefined outpu exception
    :param type_exc: system param
    :param value: system param
    :param traceback: system param
    :return: custom output exception
    """
    del traceback
    print(colored('ERROR: {type}({val})'.format(type=str(type_exc).split("'")[1],
                                                val=value), 'red'))


def check_brackets(expression):
    """
    function, which check expression to correct brackets
    :param expression: basic expression
    :return: 'False' if brackets is not correct
             'True' if brackets is correct
    """
    brackets_count = 0
    for i in expression:
        if i == '(':
            brackets_count += 1
        elif i == ')':
            brackets_count -= 1
        elif brackets_count is -1:
            raise SyntaxError("brackets aren't balanced")
    if brackets_count != 0:
        raise SyntaxError("brackets aren't balanced")


def find_length_operator(expression, position):
    """
    function, which define length current operator
    :param expression: basic expression
    :param position: position in basic expression
    :return: length operator
    """
    supposed_operator = expression[position: position + 2]
    for key in OPERATORS:
        if supposed_operator in OPERATORS[key]:
            return 2
    return 1


def priority(oper):
    """
    function, which determine priority operator
    :param oper: current operator
    :return: priority (from 1 to 6)
    """
    for key in OPERATORS:
        if oper in OPERATORS[key]:
            return key
    return -1


def is_operator(operator):
    """
    function, which determine operator
    :param operator: current operator
    :return: 'True' if operator exists, else 'False'
    """
    flag = False
    for key in OPERATORS:
        if operator in OPERATORS[key]:
            flag = True
    return flag


def isunary(cur_op):
    """
    function, which determine unary operator
    :param cur_op: current operator
    :return: 'True' if operator unary, else 'False'
    """
    if cur_op in OPERATORS[3]:
        return True
    return False


def checker_hidden_multiplication(expression):
    """
    function, which detected space, where may be multiplication sign
    :param expression: basic expression
    :return: amended basic expression
    """
    i = 0
    while i < len(expression) - 1:
        str1 = expression[i:i + 2]
        if re.match(r'\)\(', str1) or \
                re.match(r'\)[\w]', str1) or \
                re.match(r'[\w]\(', str1) or \
                re.match(r'\d[A-Za-z]', str1):
            expression = expression[0:i + 1] + '*' + expression[i + 1:]
        i += 1
    return expression


def check_many_unary(expression):
    """
    function, which chaks and removes multiple unary signs in basic expression
    :param expression: basic expression
    :return: amended basic expression
    """
    i = 1
    while i < len(expression):
        past_expr = expression[i - 1]
        cur_expr = expression[i]
        if (past_expr in ['-', '+']) and (cur_expr in ['-', '+']):
            if past_expr == '-' and cur_expr == '-':
                unary_op = '+'
            elif past_expr == '+' and cur_expr == '+':
                unary_op = '+'
            else:
                unary_op = '-'
            expression = expression[:i - 1] + unary_op + expression[i + 1:]
            i -= 1
        i += 1
    return expression


def left_assoc(cur_op):
    """
    function, which check left associative operator
    :param cur_op: current operator
    :return: 'True' if operator is left associative, else 'False'

    """
    return False if cur_op in ['**', '^'] else True


def isfloat(operand):
    """
    function, which determine operand is float
    :param operand: operand
    :return: 'True' if operand is float, else 'False'
    """
    try:
        float(operand)
        return True
    except ValueError:
        return False
