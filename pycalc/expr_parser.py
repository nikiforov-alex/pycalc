# -*- coding: utf-8 -*-
"""
Main module, which implements calculation expression
"""
import operator as op
from pycalc.const_func_parser import calculate_const, calculate_func

from pycalc.pycalc_helper import check_brackets, checker_hidden_multiplication, \
    check_many_unary, is_operator, find_length_operator, isunary, left_assoc, priority, \
    isfloat, MODULES


def process_operation(num_stack, oper):
    """
    function, which calculate simplest expression
    :param num_stack:
    :param oper:
    :return:
    """
    right = str(num_stack.pop())
    left = str(num_stack.pop())
    right = int(right) if right.isdigit() else float(right)
    left = int(left) if left.isdigit() else float(left)
    dict1 = {'+': op.add,
             '-': op.sub,
             '*': op.mul,
             '/': op.truediv,
             '//': op.floordiv,
             '%': op.mod,
             '<=': op.le,
             '>=': op.ge,
             '<': op.lt,
             '>': op.gt,
             '<>': op.ne,
             '!=': op.ne,
             '==': op.eq,
             '**': op.pow,
             '^': op.pow}
    num_stack.append(dict1[oper](left, right))


def calculate(expression):
    """
    main function, which calculate basic expression
    :param expression: basic expression
    :return: calculated expression
    """
    check_brackets(expression)
    expression = check_on_function(expression)
    expression = checker_hidden_multiplication(expression)
    expression = check_many_unary(expression)
    may_unary = True
    unary_minus = False
    operation_stack = []
    numbers_stack = []
    i = 0
    while i < len(expression):
        if expression[i] != ' ':
            if expression[i] == '(':
                operation_stack.append('(')
                may_unary = True
            elif expression[i] == ')':
                while operation_stack[-1] != '(':
                    process_operation(numbers_stack, operation_stack.pop())
                operation_stack.pop()
                may_unary = False
            elif is_operator(expression[i]) or is_operator(expression[i:i + 2]):
                expression, i, may_unary, unary_minus, operation_stack, \
                numbers_stack = actions_when_find_operator(expression, i, (may_unary, unary_minus),
                                                           operation_stack, numbers_stack)
            else:
                expression, i, may_unary, \
                unary_minus = action_if_operator_const(expression, i, unary_minus, numbers_stack)
        i += 1
    while operation_stack:
        process_operation(numbers_stack, operation_stack.pop())
    if len(numbers_stack) != 1:
        raise ArithmeticError('expression not valid')
    else:
        return numbers_stack[0]


def actions_when_find_operator(expression, i, minus, operation_stack, numbers_stack):
    """
    actions when find operator
    :param expression: base expression
    :param i: count
    :param may_unary: may unary
    :param operation_stack: operation stack
    :param numbers_stack: numbers stack
    :return:
    """
    minus = list(minus)
    length_operator = find_length_operator(expression, i)
    cur_op = expression[i:i + length_operator]
    i = i - 1 + length_operator
    if minus[0] and isunary(cur_op):
        minus[1] = True
    else:
        while operation_stack and \
                (left_assoc(cur_op) and
                 priority(operation_stack[-1]) >= priority(cur_op) or not
                 left_assoc(cur_op) and
                 priority(operation_stack[-1]) > priority(cur_op)):
            process_operation(numbers_stack, operation_stack.pop())
        operation_stack.append(cur_op)
        minus[0] = True
    return expression, i, minus[0], minus[1], operation_stack, numbers_stack


def action_if_operator_const(expression, i, unary_minus, numbers_stack):
    """
    function, which parse expression, if expression contains constants
    :param expression: main expression
    :param i: count
    :param unary_minus: True/False
    :param numbers_stack: stack, for numbers
    :return: calculated values
    """
    operand = ''
    flag = False
    while i < len(expression) and \
            (expression[i].isdigit() or expression[i] == '.'):
        operand += expression[i]
        i += 1
        flag = True
    i -= 1 if flag is True else 0
    if isfloat(operand):
        if unary_minus:
            if operand.isdigit():
                operand = int(operand)
                operand = -operand
            else:
                operand = float(operand)
                operand = -operand
            unary_minus = False
        numbers_stack.append(operand)
    else:
        while i < len(expression) and (expression[i].isalpha()):
            operand += expression[i]
            i += 1
        i -= 1
        operand = calculate_const(operand)
        numbers_stack.append(operand)
    may_unary = False
    return expression, i, may_unary, unary_minus


def check_on_function(expression):
    """
    this function check expression for availability function
    :param expression: base expression
    :return: calculated expression
    """
    i = 0
    begin_func = 0
    end_func = 0
    may_func = ''
    args = []
    while i < len(expression):
        if expression[i].isalpha():
            if len(may_func) == 0:
                begin_func = i
            may_func += expression[i]
        elif (expression[i].isalnum() or expression[i] == '_') and len(may_func) > 0:
            may_func += expression[i]
        elif expression[i] == '(' and len(may_func) > 0:
            i += 1
            brackets_correct = 0
            arg = ''
            while True:
                if expression[i] == ')' and brackets_correct == 0:
                    args.append(float(calculate(arg.strip())))
                    arg = ''
                    end_func = i
                    break
                if expression[i] == ')':
                    brackets_correct -= 1
                    arg += expression[i]
                elif expression[i] == '(':
                    brackets_correct += 1
                    arg += expression[i]
                elif expression[i] == ',' and brackets_correct == 0:
                    args.append(float(calculate(arg.strip())))
                    arg = ''
                else:
                    arg += expression[i]
                i += 1
            interval = (begin_func, end_func)
            expression, i = calculate_func(may_func, interval, expression,
                                           MODULES, *args)
            args = []
            may_func = ''
        else:
            may_func = ''
        i += 1
    return expression


if __name__ == '__main__':
    print(calculate('sin(e)+sin(e)'))
