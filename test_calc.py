from nose.tools import assert_equal
from parameterized import parameterized

import unittest
import math

from pycalc.expr_parser import calculate

testdata_unary = ['2+2',
                  "-13",
                  "6-(-13)",
                  "1---1",
                  "-+---+-1",
                  "+-+----++3+3"
                  ]

testdata_priority = ["1+2*2",
                     "1+(2+3*2)*3",
                     "10*(2+1)",
                     "10**(2+1)",
                     "100/3**2",
                     "100/3%2**2"]

testdata_func_const = [("pi+e", '5.859874482048838'),
                       ("log(e)", '1.0'),
                       ("sin(pi/2)", '1.0'),
                       ("log10(100)", '2.0'),
                       ("sin(pi/2)*111*6", '666.0'),
                       ("2*sin(pi/2)", '2.0')]

testdata_error = ["",
                  "+",
                  "1-",
                  "1 2",
                  "ee",
                  "==7",
                  "1 * * 2",
                  "1 + 2(3 * 4))",
                  "*2(2*(2+2))",
                  "((1+2)",
                  "1 + 1 2 3 4 5 6 ",
                  "log100(100)",
                  "------",
                  "5 > = 6",
                  "5 / / 6",
                  "6 < = 6",
                  "6 * * 6",
                  "((((("]

test_associative = ["102%12%7",
                    "100/4/3",
                    "2**3**4"]


def test_hidden_multy():
    assert calculate("2(2+2)") == 8


@parameterized(testdata_unary)
def test_unary(expression):
    assert (calculate(expression) == eval(expression))


@parameterized(test_associative)
def test_associative(expression):
    assert (eval(expression) == calculate(expression))


@parameterized(testdata_priority)
def test_priority(expression):
    assert (eval(expression) == calculate(expression))


@parameterized(testdata_func_const)
def test_func_const(expression, ans):
    assert str(calculate(expression)) == ans


@parameterized(testdata_error)
def test_error(expression):
    try:
        ans = calculate(expression)
    except Exception:
        assert True
    else:
        assert False
