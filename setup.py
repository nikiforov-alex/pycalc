from setuptools import setup
from os.path import join, dirname
import pycalc

setup(
    name='pycalc',
    version=pycalc.__version__,
    packages=['pycalc'],
    long_description=open(join(dirname(__file__), 'README.txt')).read(),
    test_suite='tests',
    py_modules=['main',],
    entry_points={
        'console_scripts':
            ['pycalc = main:main']
        }, install_requires=['pytest']
)