import sys

from pycalc.argparser import create_parser
from pycalc.expr_parser import calculate
from pycalc.pycalc_helper import MODULES, my_exchandler


def main():
    sys.excepthook = my_exchandler
    args = create_parser()
    expression = args.expression
    modules = args.use_modules
    if modules:
        MODULES.extend(modules)
    print(calculate(expression))
